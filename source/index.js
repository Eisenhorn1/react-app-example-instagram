// Core
import React from 'react';
import ReactDOM from 'react-dom';

//pages
import {Instagram} from './pages/Instagram';

// Theme
import './theme/init';

ReactDOM.render(<Instagram />, document.getElementById('app'));
