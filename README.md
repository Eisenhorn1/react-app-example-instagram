<h1 align="center">
    React app example
</h1>
<br>

### Для запуска проекта

1. Node.js (v8.11)
2. Установить [Yeoman](http://yeoman.io/) и [генератор проекта компании Lectrum](https://github.com/Lectrum/generator-ui):

```bash
npm install -g yo @lectrum/generator-ui
```

3. 

```bash
yo @lectrum/ui
```